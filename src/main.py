import os
import sys
import time
import logging

from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from minio import Minio


class Event(LoggingEventHandler):
    def on_created(self, event):
        minio(event.src_path, "products")


def minio(path, bucket):
    client = Minio(
        "minio:9000",
        access_key="admin",
        secret_key="password",
        secure=False,
    )

    found = client.bucket_exists(bucket)
    if not found:
        client.make_bucket(bucket)
    else:
        print(f"Bucket '{bucket}' already exists")

    filename = os.path.basename(path)

    client.fput_object(
        bucket, filename, path
    )
    print(
        f"{path} is successfully uploaded as "
        f"object '{filename}' to bucket '{bucket}'."
    )


if __name__ == "__main__":
    print('STARTING LISTENING')
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    path = './images'
    event_handler = Event()
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
